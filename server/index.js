const express = require('express');
const path = require('path');

const app = express();

app.use(express.static('public'));

// routing

// app.get('/test', function(req, res) {
//     console.log('test')
//     const test = path.join(__dirname, '../public/cars.html')
//     const test2 = (__dirname, '../public/index.html')
//     console.log(test)
//     console.log(test2)
//     res.sendFile(path.join(__dirname, '../public/index.html'));
// });

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '/index.html'));
});

app.get('/cars', function(req, res) {
    res.sendFile(path.join(__dirname, '../public/cars.html'));
});

app.listen(8000, '0.0.0.0', () => {
    console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", 8000);
});